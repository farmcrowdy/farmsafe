$('.slider_').owlCarousel({
  loop:true,
  margin:0,
  nav:false,
  loop:true,
  autoplay:true,
  items:1,
  responsive:{
    1400:{
        items:1,
    }
  }
});

// Get the modal
var modal = document.getElementById("myModal");

// Get all links that opens the modal
let openModalBtn = document.querySelectorAll(".open_modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_modal")[0];

if(modal){
  for (const element of openModalBtn) {
    // console.log(element);
        // When a user's name is clicked, open the modal 
      element.onclick = ()=> {
    // console.log(element);

      modal.style.display = "block";
    }
  }
  

  // When the user clicks on <span> (x), close the modal
  span.onclick = ()=> {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = (event)=> {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}

