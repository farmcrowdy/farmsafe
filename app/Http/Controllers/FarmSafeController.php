<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Farmsafe;

class FarmSafeController extends Controller
{
    public function index() {
      return view('home');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
        'name' => 'required|max:255',
        'company' => 'max:255',
        'phone' => 'required',
        'email' => 'required',
        'gender' => 'required',
        'occupation' => 'required',
        'personalInsurance' => 'required',
        'cropInsurance' => 'required'
       ]);

       $createRecord = Farmsafe::create([
         'name' => $request->name,
         'company' => $request->company,
         'email' => $request->email,
         'phone' => $request->phone,
         'gender' => $request->gender,
         'occupation' => $request->occupation,
         'personal_insurance_premium' => $request->personalInsurance,
         'crop_insurance_premium' => $request->cropInsurance
       ]);

       if($createRecord){
         return redirect('/');
       }
   }
}
