<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farmsafe extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'email', 'company', 'phone', 'gender',
        'occupation', 'personal_insurance_premium', 'crop_insurance_premium'
    ];
}
